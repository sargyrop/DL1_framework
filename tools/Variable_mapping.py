import collections

mapping = {
    "eventnb": "eventNumber",
    "jet_LabDr_HadF": "HadronConeExclTruthLabelID",
    "jet_ip2_cu": "IP2D_cu",
    "e_frac_vtx1": "JetFitterSecondaryVertex_energyFraction",
    "jet_rnnip_pb": "rnnip_pb",
    "jet_rnnip_pc": "rnnip_pc",
    "nTrk_vtx1": "JetFitterSecondaryVertex_nTracks",
    "jet_jf_nvtx": "JetFitter_nVTX",
    "jet_jf_nvtx1t": "JetFitter_nSingleTracks",
    "jet_sv1_n2t": "SV1_N2Tpair",
    "jet_rnnip_pu": "rnnip_pu",
    "e_first_vtx": "JetFitterSecondaryVertex_energy",
    # "jet_rnnip_ptau": "rnnip_ptau",
    "jet_sv1_efc": "SV1_efracsvx",
    "jet_jf_m": "JetFitter_mass",
    "jet_sv1_Lxy": "SV1_Lxy",
    "JF_Lxy1": "JetFitterSecondaryVertex_displacement2d",
    # "jet_mu_dR": "SMT_dR",
    "vtx1_MaxTrkRapidity": "JetFitterSecondaryVertex_maximumTrackRelativeEta",
    "jet_sv1_deltaR": "SV1_deltaR",
    "MaxTrkRapidity": "maximumTrackRelativeEta",
    "jet_sv1_ntrkv": "SV1_NGTinSvx",
    # "jet_mu_scatneighsignif": "SMT_scatneighsignif",
    "jet_pt_orig": "pt_btagJes",
    "MinTrkRapidity": "minimumTrackRelativeEta",
    "jet_jf_efc": "JetFitter_energyFraction",
    # "jet_mu_d0": "SMT_mu_d0",
    # "jet_mu_z0": "SMT_mu_z0",
    "jet_ip2": "IP2D_bu",
    "jet_ip3": "IP3D_bu",
    "closestVtx_L3D": "JetFitterSecondaryVertex_displacement3d",
    "jet_ip2_c": "IP2D_bc",
    # "jet_mu_mombalsignif": "SMT_mombalsignif",
    "jet_ip3_c": "IP3D_bc",
    "mass_first_vtx": "JetFitterSecondaryVertex_mass",
    "jet_sv1_L3d": "SV1_L3d",
    "jet_eta_orig": "eta_btagJes",
    # "jet_mu_pt": "SMT_mu_pt",
    # "jet_mu_pTrel": "SMT_pTrel",
    # "jet_mu_qOverPratio": "SMT_qOverPratio",
    "vtx1_AvgTrkRapidity": "JetFitterSecondaryVertex_averageTrackRelativeEta",
    "jet_jf_sig3d": "JetFitter_significance3d",
    "vtx1_MinTrkRapidity": "JetFitterSecondaryVertex_minimumTrackRelativeEta",
    "AvgTrkRapidity": "averageTrackRelativeEta",
    "jet_sv1_m": "SV1_masssvx",
    "jet_jf_dR": "JetFitter_deltaR",
    "jet_sv1_sig3d": "SV1_significance3d",
    "jet_jf_n2t": "JetFitter_N2Tpair",
    "jet_jf_ntrkAtVx": "JetFitter_nTracksAtVtx",
    "jet_ip3_cu": "IP3D_cu",
    "jet_jf_dphi": "JetFitter_deltaphi",
    "jet_jf_deta": "JetFitter_deltaeta",
    "jet_jf_mUncorr": "JetFitter_massUncorr",
    "jet_jf_dRFlightDir": "JetFitter_dRFlightDir",
    "jet_sv1_normdist": "SV1_dstToMatLay",
    "jet_pt": "pt",
    "jet_eta": "eta",
    "jet_JVT": "bTagJVT",
    "jet_aliveAfterOR": "jet_aliveAfterOR",
    "jet_mv2c10": "MV2c10_discriminant",
    "jet_ip2d_pu": "IP2D_pu",
    "jet_ip2d_pc": "IP2D_pc",
    "jet_ip2d_pb": "IP2D_pb",
    #"jet_ip2d_ntrk": "IP2D_nTrks", #Not in IDTIDE Ntuple
    "jet_ip3d_pu": "IP3D_pu",
    "jet_ip3d_pc": "IP3D_pc",
    "jet_ip3d_pb": "IP3D_pb",
    "jet_ip3d_ntrk": "IP3D_nTrks", 
    "jet_dl1_pu": "DL1_pu",
    "jet_dl1_pc": "DL1_pc",
    "jet_dl1_pb": "DL1_pb",
    "jet_dl1rnn_pu": "DL1r_pu",
    "jet_dl1rnn_pc": "DL1r_pc",
    "jet_dl1rnn_pb": "DL1r_pb",
    "jet_dl1mu_pu": "DL1rmu_pu",
    "jet_dl1mu_pc": "DL1rmu_pc",
    "jet_dl1mu_pb": "DL1rmu_pb",
    "jet_nBHadr": "GhostBHadronsFinalCount",
    "jet_nCHadr": "GhostCHadronsFinalCount",
    "jet_bH_pt": "GhostBHadronsFinalPt",
    "avgmu": "averageInteractionsPerCrossing",
    "actmu": "actualInteractionsPerCrossing",
    "mcwg": "mcEventWeight",
    #"n_cluster_layer0_r004": "nHitL0r004", #Some cluster variables to add as inputs to DL1r
    #"n_cluster_layer1_r004": "nHitL1r004",
    #"n_cluster_layer2_r004": "nHitL2r004",
    #"n_cluster_layer3_r004": "nHitL3r004",

}

track_mapping = collections.OrderedDict()
track_mapping['jet_trk_chi2']           = 'chiSquared'
track_mapping['jet_trk_ndf']            = 'numberDoF'
track_mapping['jet_trk_ip3d_d0']        = 'IP3D_signed_d0'
track_mapping['jet_trk_ip3d_d02D']      = 'IP2D_signed_d0'
track_mapping['jet_trk_ip3d_z0']        = 'IP3D_signed_z0'
track_mapping['jet_trk_ip3d_d0sig']     = 'IP3D_signed_d0_significance'
track_mapping['jet_trk_ip3d_z0sig']     = 'IP3D_signed_z0_significance'
track_mapping['jet_trk_d0']             = 'btagIp_d0'
track_mapping['jet_trk_z0']             = 'btagIp_z0SinTheta'
track_mapping['jet_trk_phi']            = 'phi'
track_mapping['jet_trk_theta']          = 'theta'
track_mapping['jet_trk_qoverp']         = 'qOverP'
track_mapping['jet_trk_ip2d_grade']     = 'IP2D_grade'## Use IP3D for now???
track_mapping['jet_trk_ip3d_grade']     = 'IP3D_grade'
track_mapping['jet_trk_nInnHits']       = 'numberOfInnermostPixelLayerHits'
track_mapping['jet_trk_nNextToInnHits'] = 'numberOfNextToInnermostPixelLayerHits'
track_mapping['jet_trk_nsharedBLHits']  = 'numberOfInnermostPixelLayerSharedHits'
track_mapping['jet_trk_nsplitBLHits']   = 'numberOfInnermostPixelLayerSplitHits'
track_mapping['jet_trk_nPixHits']       = 'numberOfPixelHits'
track_mapping['jet_trk_nPixHoles']      = 'numberOfPixelHoles'
track_mapping['jet_trk_nsharedPixHits'] = 'numberOfPixelSharedHits'
track_mapping['jet_trk_nsplitPixHits']  = 'numberOfPixelSplitHits'
track_mapping['jet_trk_nSCTHits']       = 'numberOfSCTHits'
track_mapping['jet_trk_nSCTHoles']      = 'numberOfSCTHoles'
track_mapping['jet_trk_nsharedSCTHits'] = 'numberOfSCTSharedHits'
track_mapping['jet_trk_pt']             = 'pt'
track_mapping['jet_trk_eta']            = 'eta'

dtype_list = [
    ('chiSquared', '<f4'),
    ('numberDoF', '<f4'),
    ('IP3D_signed_d0', '<f4'),
    ('IP2D_signed_d0', '<f4'),
    ('IP3D_signed_z0', '<f4'),
    ('IP3D_signed_d0_significance', '<f4'),
    ('IP3D_signed_z0_significance', '<f4'),
    ('btagIp_d0', '<f4'),
    ('btagIp_z0SinTheta', '<f4'),
    ('phi', '<f4'),
    ('theta', '<f4'),
    ('qOverP', '<f4'),
    ('IP2D_grade', '<i4'),
    ('IP3D_grade', '<i4'),
    ('numberOfInnermostPixelLayerHits', 'u1'),
    ('numberOfNextToInnermostPixelLayerHits', 'u1'),
    ('numberOfInnermostPixelLayerSharedHits', 'u1'),
    ('numberOfInnermostPixelLayerSplitHits', 'u1'),
    ('numberOfPixelHits', 'u1'),
    ('numberOfPixelHoles', 'u1'),
    ('numberOfPixelSharedHits', 'u1'),
    ('numberOfPixelSplitHits', 'u1'),
    ('numberOfSCTHits', 'u1'),
    ('numberOfSCTHoles', 'u1'),
    ('numberOfSCTSharedHits', 'u1'),
    ('pt', '<f4'),
    ('eta', '<f4'),
    ('deta', '<f4'),
    ('dphi', '<f4'),
    ('dr', '<f4'),
    ('ptfrac', '<f4')
]

var_conv_oldDl1 = {
    "eventnb": "eventNumber",
    "label": "HadronConeExclTruthLabelID",
    "ip2_cu": "IP2D_cu",
    "e_frac_vtx1": "JetFitterSecondaryVertex_energyFraction",
    "rnnip_pb": "rnnip_pb",
    "rnnip_pc": "rnnip_pc",
    "nTrk_vtx1": "JetFitterSecondaryVertex_nTracks",
    "jf_nvtx": "JetFitter_nVTX",
    "jf_nvtx1t": "JetFitter_nSingleTracks",
    "sv1_n2t": "SV1_N2Tpair",
    "rnnip_pu": "rnnip_pu",
    "e_first_vtx": "JetFitterSecondaryVertex_energy",
    "rnnip_ptau": "rnnip_ptau",
    "sv1_efrc": "SV1_efracsvx",
    "jf_mass": "JetFitter_mass",
    "sv1_Lxy": "SV1_Lxy",
    "JF_Lxy1": "JetFitterSecondaryVertex_displacement2d",
    "mu_dR": "SMT_dR",
    "vtx1_MaxTrkRapidity": "JetFitterSecondaryVertex_maximumTrackRelativeEta",
    "sv1_dR": "SV1_deltaR",
    "MaxTrkRapidity": "maximumTrackRelativeEta",
    "sv1_ntkv": "SV1_NGTinSvx",
    "mu_scatneighsignif": "SMT_scatneighsignif",
    "pt_uCalib": "pt_btagJes",
    "MinTrkRapidity": "minimumTrackRelativeEta",
    "jf_efrc": "JetFitter_energyFraction",
    "mu_d0": "SMT_mu_d0",
    "ip2": "IP2D_bu",
    "ip3": "IP3D_bu",
    "closestVtx_L3D": "JetFitterSecondaryVertex_displacement3d",
    "ip2_c": "IP2D_bc",
    "mu_mombalsignif": "SMT_mombalsignif",
    "ip3_c": "IP3D_bc",
    "mass_first_vtx": "JetFitterSecondaryVertex_mass",
    "sv1_L3d": "SV1_L3d",
    "eta_abs_uCalib": "absEta_btagJes",
    "mu_pTrel": "SMT_pTrel",
    "mu_qOverPratio": "SMT_qOverPratio",
    "vtx1_AvgTrkRapidity": "JetFitterSecondaryVertex_averageTrackRelativeEta",
    "jf_sig3": "JetFitter_significance3d",
    "vtx1_MinTrkRapidity": "JetFitterSecondaryVertex_minimumTrackRelativeEta",
    "AvgTrkRapidity": "averageTrackRelativeEta",
    "sv1_mass": "SV1_masssvx",
    "jf_dR": "JetFitter_deltaR",
    "sv1_sig3": "SV1_significance3d",
    "jf_n2tv": "JetFitter_N2Tpair",
    "jf_ntrkv": "JetFitter_nTracksAtVtx",
    "ip3_cu": "IP3D_cu"
}
