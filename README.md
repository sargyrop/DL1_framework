# DL1 Framework

Instructions for the original package can be found [here](https://gitlab.cern.ch/atlas-flavor-tagging-tools/DL1_framework)

The instructions below are used for **converting IDTIDE ntuples to h5 files** using the script provided in
`tools/convert_fromROOT.py`.

This fork contains some changes needed for the IDTIDE ntuples (thanks to Todd Huffman for providing them!).


### Installation

```
git clone ssh://git@gitlab.cern.ch:7999/sargyrop/DL1_framework.git
```

If `uproot` is not installed, do:

```
pip install uproot --user
```

### Set up environment and run conversion

```
source /cvmfs/sft.cern.ch/lcg/views/setupViews.sh LCG_98python3 x86_64-centos7-gcc8-opt
python3 tools/convert_fromROOT.py --even --ujets --single-file --input /path/to/input/* --output /path/to/output_dir 
```

The script `run.sh` can be used to automatically run the conversion for b,c, and light jets. Run with
```
source run.sh
```
