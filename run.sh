#!/usr/bin/env bash

if [[ $_ == $0 ]] ; then
  echo "This script should be sourced"
  exit 0
fi

INITDIR=/nfs/dust/atlas/user/sargyrop/FTAG/user.thuffman.IDTIDE-Ntuple.QSP_s3582.Zprime_EXT0.V01_Akt4EMTo
OUTDIR=/nfs/dust/atlas/user/sargyrop/FTAG/OutputH5

runCommand() {
  #echo "python3 tools/convert_fromROOT.py $1 $2 --single-file --input $INITDIR/*54.root --output $OUTDIR"
  python3 tools/convert_fromROOT.py $1 $2 --single-file --input $INITDIR/*54.root --output $OUTDIR
}

source /cvmfs/sft.cern.ch/lcg/views/setupViews.sh LCG_98python3 x86_64-centos7-gcc8-opt
 
jetTypes=("--bjets" "--cjets" "--ujets")
parity=("--odd")

for par in ${parity} ; do
  for jet in ${jetTypes} ; do
    if [[ "$par" == "--odd" ]] ; then
      runCommand $par ""
      break
    fi 
    runCommand $par $jet
  done 
done

return
