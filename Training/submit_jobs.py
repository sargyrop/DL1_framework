import subprocess
import sys
import os
sys.path.append("../tools")
import bsub_tools as bt

epochs = 300
filepath = "/gpfs/slac/atlas/fs1/d/mg294/DL1_training_files"
dictpath = filepath
outpath = "/gpfs/slac/atlas/fs1/d/mg294/DL1_training_files/DL1_models"
vardictpath = "/gpfs/slac/staas/fs1/g/jupyter/mg294/notebooks/dl1_jupyter/\
PFlow_trainings"
command = "python train.py --use_gpu \
-c configs/DL1r_4M--normal_hybridconfig.json -e %i -f %s --dict_path %s\
 -o %s --var_dict_path %s" % (epochs, filepath, dictpath, outpath, vardictpath)

# # make a directory to store training output in
# try:
#     # Create target Directory
#     os.mkdir("../networks/"+name)
#     print("Directory " , name ,  " Created ")
# except:
#     # Don't retrain if this hyperparameter set already exists
#     print("Directory " , name ,  " already exists")
#     continue


jobs = bt.bsub(command, sh_name='run_test-bsub.job')
process = jobs.Write()
print(process)

# # checking if the job already exists or failed
# if process == 0:
#     exit(0)
# # Launching the jobs
# else:
#     test = subprocess.Popen(process, shell=True)
#     test.wait()
